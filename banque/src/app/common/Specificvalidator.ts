import { AbstractControl, ValidationErrors, Validators } from "@angular/forms"
import { AccountModel } from "src/app/models/model-account";
import { ACCOUNTSLIST } from "./mock-account";

export class SpecificValidator {
    static containService:any;
    static service:AccountModel[]=ACCOUNTSLIST;
    constructor(){}
//-------------------------------------------------------------------------------------------
   static getAllaccount(){
        SpecificValidator.containService=SpecificValidator.service;
        return SpecificValidator.containService;
    }
    static shouldContainUppercaseCharacter(control:AbstractControl): ValidationErrors | null {
      let hasUppercaseCharacter=/[A-Z]/.test(control.value);
      if(!hasUppercaseCharacter){
       return {shouldContainUppercaseCharacter : true};
    }else{   return null;}
    }
//--------------------------------------------------------------------------------------------
    static shouldContainLowerCaseCharacter(control:AbstractControl): ValidationErrors | null {
        let LowerCasecaseCharacter=/[a-z]/.test(control.value);
        if(!LowerCasecaseCharacter){
         return {shouldContainLowerCaseCharacter : true};
      }else{   return null;}
      }
//--------------------------------------------------------------------------------------------
      static shouldContainNumber(control:AbstractControl): ValidationErrors | null {
        let shouldContainNumber=/[0-9]/.test(control.value);
        if(!shouldContainNumber){
         return {shouldContainNumber : true};
      }else{   return null;}
      }
//--------------------------------------------------------------------------------------------      
      static shouldContainSpecialCharacter(control:AbstractControl): ValidationErrors | null {
        let specialCharacter=/[-+_!@#$%^&*.,;:~/"'()|<>={}?]/.test(control.value);
        if(!specialCharacter){
         return {shouldContainSpecialCharacter : true};
      }else{   return null;}
      }
//----------------------------------------------------------------------------------------------
    static shoudNotContainSpace(control:AbstractControl):ValidationErrors| null{
        if((control.value as string).indexOf(" ")>=0){
            return {shoudNotContainSpace:true }
        }
        return null;
    }
//----------------------------------------------------------------------------------------------
    static shoudBePositif(control:AbstractControl):ValidationErrors | null{
        if(control.value<20){
            return {shoudBePositif:true}
        }
        return null;
    }
//-----------------------------------------------------------------------------------------
static atLeastTenEuro(control:AbstractControl):ValidationErrors | null{
    if(control.value<10){
        return {atLeastTenEuro: true};
    }
    return null;
}
//------------------------------------------------------------------------------------------

static accountIsExist(control:AbstractControl):ValidationErrors |null{
    const index =SpecificValidator.getAllaccount().findIndex((acc: { accountNumber: any; }) =>acc.accountNumber==Number(control.value));
    if(index!=(-1)){
        return null;
    }else{return { accountIsExist : true}}
}

}

