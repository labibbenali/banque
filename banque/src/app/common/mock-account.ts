
import {AccountModel} from '../models/model-account'

export const ACCOUNTSLIST :AccountModel[]=[
{   id:1,
    firstName:"john",
    lastName:"Doe",
    email:"john.doe@gmail.com",
    password:"John>123",
    amount:150,
    accountNumber:800
},
{   id:2,
    firstName:"Abbie",
    lastName:"Abby",
    email:"Abby1234@gmail.com",
    password:"abY%123",
    amount:120,
    accountNumber:8500
},
{   id:3,
    firstName:"Abram",
    lastName:"Abe",
    email:"Abram.abe@gmail.com",
    password:"Abraham#123",
    amount:420,
    accountNumber:7800
    
},
{   id:4,
    firstName:"Becky",
    lastName:"Becca",
    email:"Becca@gmail.com",
    password:"becBecky&123",
    amount:260,
    accountNumber:9700
}

]