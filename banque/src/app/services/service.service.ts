import { Injectable } from '@angular/core';
import { ACCOUNTSLIST } from '../common/mock-account';
import { AccountModel } from '../models/model-account';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
private allAccount:AccountModel[]=ACCOUNTSLIST;
data!: any;


getAllAccount(){
  return this.allAccount;
}
addAccount(acc:AccountModel):void{
  this.allAccount.unshift(acc);
}

getaccountByAccountNumber(accNumber:number):any{
const account=this.getAllAccount().find( account => account.accountNumber === accNumber);
if(account){
  return account;
}else{  throw "Account not exist";}
}

}