

export interface AccountModel{
    id:number;
    firstName:string;
    lastName:string;
    email:string;
    password:string;
    amount:number;
    accountNumber:number;

}