import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SpecificValidator } from 'src/app/common/Specificvalidator';
import { AccountModel } from 'src/app/models/model-account';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'upload-funds',
  templateUrl: './upload-funds.component.html',
  styleUrls: ['./upload-funds.component.scss']
})
export class UploadFundsComponent implements OnInit {
  msg!:string;
  form:any=new FormGroup({
    accountNumber:new FormControl("",[Validators.required,SpecificValidator.accountIsExist]),
    amountToUpload: new FormControl("",[
      Validators.required,
      SpecificValidator.atLeastTenEuro
    ])
  });

  accountObj!:AccountModel;
  constructor(private service : ServiceService) { }
//--------------------------------------------------------------
  ngOnInit(): void {
    this.accountObj=this.service.data;
    if(this.accountObj){
      
  (<FormGroup>this.form).patchValue({
    accountNumber :this.accountObj.accountNumber,
  })
}
  }

//--------------------------------------------------------------
get accountNumber(){return this.form.get("accountNumber");}
get amountToUpload(){ return this.form.get("amountToUpload");}
//---------------------------------------------------------------
accountExist(){
this.msg="";
}
//--------------------------------------------------------------------------------------
upload(){
  if(this.form.status=="INVALID"){
    this.msg="please verify the input field";}else{


const index=this.service.getAllAccount().findIndex(account =>account.accountNumber==(this.form.value.accountNumber))

if(index!=(-1)){
  this.service.getAllAccount()[index].amount+=Number(this.form.value.amountToUpload);
this.msg="Done!"
this.form.reset({ firstaccountNumberName:"", lasamountToUploadtName:""});

}else{
  this.msg="please verify your input field";
}
}
}
}
