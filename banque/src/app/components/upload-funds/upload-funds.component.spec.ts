import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadFundsComponent } from './upload-funds.component';

describe('UploadFundsComponent', () => {
  let component: UploadFundsComponent;
  let fixture: ComponentFixture<UploadFundsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadFundsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadFundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
