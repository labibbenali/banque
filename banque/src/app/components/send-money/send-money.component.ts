import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountModel } from 'src/app/models/model-account';
import { ServiceService } from 'src/app/services/service.service';
@Component({
  selector: 'app-send-money',
  templateUrl: './send-money.component.html',
  styleUrls: ['./send-money.component.scss']
})
export class SendMoneyComponent implements OnInit {
  msg!:string;
  notExistAcc!:boolean;
  isDisabled:boolean=true;
  notExist!:boolean;
  accountObj!:AccountModel;

  form:any=new FormGroup({
    firstNameSender: new FormControl(""),
    lastNameSender: new FormControl(""),
    numberOfAccount:new FormControl("",
    [
      Validators.required
    ]),

    amountToSend:new FormControl("",
    [
      Validators.required,
      
    ]),
  
    firstNameReceiver: new FormControl(""),
    lastNameReceiver: new FormControl(""),
    numberOfAccountReceiver:new FormControl("",[
      Validators.required,
    ])

  });

  constructor(private service :ServiceService) { }

  ngOnInit(): void {
    this.accountObj=this.service.data;
    if(this.accountObj){
      this.notExist=true;
  (<FormGroup>this.form).patchValue({
    firstNameSender :this.accountObj.firstName,
    lastNameSender:this.accountObj.lastName,
    numberOfAccount:this.accountObj.accountNumber,
    
  })
}
  }

  get firstNameSender(){  return this.form.get("firstNameSender");}
  get lastNameSender(){   return this.form.get("lastNameSender");}
  get amountToSend(){  return this.form.get("amountToSend") ; }
  get firstNameReceiver(){ return this.form.get("firstNameReceiver");}   
  get lastNameReceiver(){ return this.form.get("lastNameReceiver") ;   }
  get numberOfAccount(){ return this.form.get("numberOfAccount") ;   }
  get numberOfAccountReceiver(){ return this.form.get("numberOfAccountReceiver") ;  }

  verifyAccountSender(data:any){
    const getAccount=this.service.getAllAccount().find( account => account.accountNumber == data.target.value );
    this.notExist=false;
    this.msg=""
    if(getAccount){
      this.notExist=true;
      (<FormGroup>this.form).patchValue({
        firstNameSender :getAccount.firstName,
        lastNameSender:getAccount.lastName,
        numberOfAccount:getAccount.accountNumber,
      })
        }else{
          this.notExist=false;
          (<FormGroup>this.form).patchValue({
            firstNameSender :"",
            lastNameSender:"",
          })
        }
  }
//--------------------------------------------------------
verifyaccountReceiver(data:any){
  const getAccount=this.service.getAllAccount().find( account => account.accountNumber == data.target.value );
  this.notExistAcc=false;
  this.msg="";
  if(getAccount){
    this.notExistAcc=true;
      (<FormGroup>this.form).patchValue({
        firstNameReceiver :getAccount.firstName,
        lastNameReceiver:getAccount.lastName,
        numberOfAccountReceiver:getAccount.accountNumber,
      })
    }else{
      this.notExistAcc=false;
      (<FormGroup>this.form).patchValue({
        firstNamefirstNameReceiverSender :"",
        lastNameReceiver:"",
      })
    }
}
//--------------------------------------------------------------------
senMoney(){
 if(this.form.status=="INVALID"){
  this.msg="please verify the input field";
 }else{
 const account = this.service.getaccountByAccountNumber(this.form.value.numberOfAccount);
 const accN=this.form.value.numberOfAccountReceiver;
 let objindex=this.service.getAllAccount().findIndex(obj=> obj.accountNumber==accN);

  if((account.amount-this.form.value.amountToSend)<20 && (account.amount-this.form.value.amountToSend)>=0){
    this.msg="account should at least contain 20€"
 
  } else if (account.amount<this.form.value.amountToSend){ 
    this.msg="this amount is greater than what the account containt"}

else if(this.form.value.amountToSend<10){
  
  this.msg="minimum amount you can send is 10€"}
  else if(this.service.getAllAccount()[objindex].accountNumber===account.accountNumber){ 
    this.msg="the process was unsuccessful , please verify and retry";
  }
  else{this.msg="";
 account.amount-=Number(this.form.value.amountToSend);
  this.service.getAllAccount()[objindex].amount+=Number(this.form.value.amountToSend);

  this.form.reset({ firstNameSender:"", lastNameSender:"", amountToSend:"", firstNameReceiver:"", lastNameReceiver:"",
  numberOfAccount:"",numberOfAccountReceiver:"" })
  this.notExistAcc=false;
  this.notExist=false;
  this.msg="Done!";
  
}
 }
}
}




