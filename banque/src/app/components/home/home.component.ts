import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountModel } from 'src/app/models/model-account';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 // 
  account!:AccountModel;
  listAccount!:AccountModel[];

  constructor(private service:ServiceService,
    private route :ActivatedRoute,
    private router : Router ) { }

  ngOnInit(): void {
   this.listAccount=this.service.getAllAccount();
  const accountId = +this.route.snapshot.params["id"];
}

toSendMoney(account:AccountModel){
  this.service.data=account;
  this.router.navigateByUrl('sendMoney');
}
uploadMoney(account:AccountModel){
this.service.data=account;
this.router.navigateByUrl('uploadMoney');
}

}
