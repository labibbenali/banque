import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private service :ServiceService,
    private router :Router) { }

  ngOnInit(): void {
  }
  goToSendMoney(){
    this.service.data=undefined;
    this.router.navigateByUrl('sendMoney');
  }

  goToUploadmoney(){
    this.service.data=undefined;
    this.router.navigateByUrl('uploadMoney');
  }
}
