import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountModel } from 'src/app/models/model-account';
import { SpecificValidator } from '../../common/Specificvalidator';
import { ServiceService } from 'src/app/services/service.service';


@Component({
  selector: 'new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.scss']
})
export class NewAccountComponent implements OnInit {
  static accountId:number=0
  account!:AccountModel;
  isValid:boolean=false;
  msgError!:string;
  static accountNumber:number=0;
  accountBalancce:number=0;

  form:any=new FormGroup({
    firstName : new FormControl("",[
      Validators.required,
      Validators.minLength(3),
      Validators.pattern("^[a-zA-Z]+$"),
      SpecificValidator.shoudNotContainSpace
    ]),
    lastName : new FormControl("",[
      Validators.required,
      Validators.minLength(3),
      Validators.pattern("^[a-zA-Z]+$"),
      SpecificValidator.shoudNotContainSpace
    ]),
    email : new FormControl("",[
      Validators.required, 
      Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]
    ),
    password : new FormControl("",[
      Validators.required,
      Validators.minLength(6),
      SpecificValidator.shouldContainUppercaseCharacter,
      SpecificValidator.shouldContainLowerCaseCharacter,
      SpecificValidator.shouldContainNumber,
      SpecificValidator.shouldContainSpecialCharacter,
      SpecificValidator.shoudNotContainSpace,
    ]),
    initialAmount:new FormControl("",[Validators.required,SpecificValidator.shoudBePositif])
  });

  constructor(private service :ServiceService) { }

  ngOnInit(): void { }
  
  get firstName() {   return this.form.get("firstName");  }
  get lastName()  {   return this.form.get("lastName");    }
  get email()     {   return this.form.get("email");    }
  get password()  {   return this.form.get("password");    }
  get initialAmount() { return this.form.get("initialAmount");  }

  getStaticAccountNUmber(){
    return NewAccountComponent.accountNumber;
  }

  signUp(){
    if(this.form.status=="INVALID"){
      this.msgError="Please check and verify your input field";
    }else{
      NewAccountComponent.accountNumber++;
      this.isValid=true;
      NewAccountComponent.accountId++;
      this.account={
        id:NewAccountComponent.accountId,
        firstName:this.form.value.firstName,
        lastName:this.form.value.lastName,
        email:this.form.value.email,
        password:this.form.value.password,
        amount:this.form.value.initialAmount,
        accountNumber:NewAccountComponent.accountNumber
      }
      this.service.addAccount(this.account);
    
      this.form.reset({ firstName:"", lastName:"", email:"", password:"" });
      this.msgError="";
    }
 
  }
}
