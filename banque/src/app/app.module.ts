import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NewAccountComponent } from './components/new-account/new-account.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './route/route';
import { LOCALE_ID } from '@angular/core';
import localeFr from '@angular/common/locales/fr'
import { registerLocaleData } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SendMoneyComponent } from './components/send-money/send-money.component';
import { UploadFundsComponent } from './components/upload-funds/upload-funds.component';
registerLocaleData(localeFr);
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewAccountComponent,
    HeaderComponent,
    FooterComponent,
    SendMoneyComponent,
    UploadFundsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
