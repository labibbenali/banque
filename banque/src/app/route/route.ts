import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "../components/home/home.component";
import { NewAccountComponent } from "../components/new-account/new-account.component";
import { SendMoneyComponent } from "../components/send-money/send-money.component";
import { UploadFundsComponent } from "../components/upload-funds/upload-funds.component";

const routes: Routes=[
    {path:"" , component : HomeComponent},
    {path:"accountcreate", component :NewAccountComponent },
    {path:"sendMoney" , component:SendMoneyComponent},
    {path :"uploadMoney" ,  component:UploadFundsComponent}
]

@NgModule({
imports: [
    RouterModule.forRoot(routes)
],
exports :[
    RouterModule
]
})

export class AppRoutingModule{}